package com.applaunch.eventweather.Adapter

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.text.Html
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.wisdomleaftask.Model.ImagesModelItem
import com.wisdomleaftask.R
import kotlinx.android.synthetic.main.dialog_data.*
import kotlinx.android.synthetic.main.row_imagelist.view.*
import java.util.*


class ImageListAdapter(
        var context: Activity,
        var imageList: ArrayList<ImagesModelItem>
) :
        RecyclerView.Adapter<ImageListAdapter.ViewHolder>() {
    //    with the help of layout inflator pass with item layout.
    override fun onCreateViewHolder(
            parent: ViewGroup,
            p1: Int
    ): ViewHolder {
        val view = LayoutInflater.from(context)
                .inflate(R.layout.row_imagelist, parent, false)
        return ViewHolder(view)
    }

    // return list size
    override fun getItemCount(): Int {
        return imageList.size
    }

    //    set data to views
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.txt_rowimageslist_author.setText(
                context.getString(R.string.author) + "- " + imageList.get(
                        position
                ).author
        )
        holder.txt_rowimageslist_id.setText(
                context.getString(R.string.id) + "- " + imageList.get(
                        position
                ).id
        )
        holder.txt_rowimageslist_credits.setText(
                context.getString(R.string.credits) + "- " + imageList.get(
                        position
                ).url
        )
        Glide.with(context)
            .load(imageList.get(position).download_url)
            .into(holder.rounded_image)
            .onLoadStarted(context.getDrawable(R.drawable.app_icon))


        val displayMetrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels
        holder.rounded_image.layoutParams.height = height * 10 / 100
        holder.rounded_image.layoutParams.width = width * 20 / 100

        val animation = AnimationUtils.loadAnimation(context, R.anim.bouse_zoom_in)
        holder.itemView.startAnimation(animation)

        holder.itemView.setOnClickListener {
            openDialog(
                    context,
                    imageList.get(
                            position
                    ).author,
                    imageList.get(
                            position
                    ).id,
                    imageList.get(
                            position
                    ).url,
                    imageList.get(
                            position
                    ).width,
                    imageList.get(
                            position
                    ).height,
                    imageList.get(
                            position
                    ).download_url
            )
        }
    }

    // Passed the required arguments and showed the data in the custom dialog
    private fun openDialog(
            context: Activity,
            author: String,
            id: String,
            url: String,
            image_width: Int,
            image_height: Int,
            downloadUrl: String
    ) {


        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_data)

        val displayMetrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels
        dialog.image_dialog.layoutParams.height = height * 30 / 100
        dialog.image_dialog.layoutParams.width = width * 70 / 100


        Glide.with(context)
                .load(downloadUrl)
                .into(dialog.image_dialog)
                .onLoadStarted(context.getDrawable(R.drawable.app_icon))

        dialog.txt_dialog_id_title.text = context.getString(R.string.id) + " - "
        dialog.txt_dialog_id_value.text = id
        dialog.txt_dialog_author_title.text = context.getString(R.string.author) + " - "
        dialog.txt_dialog_author_value.text = author
        dialog.txt_dialog_resolution_title.text = context.getString(R.string.resolution) + " - "
        dialog.txt_dialog_resolution_value.text =
                image_width.toString() + " X " + image_height.toString()
        dialog.txt_dialog_credits_title.text = context.getString(R.string.credits) + " - "
        dialog.txt_dialog_credits_value.text = Html.fromHtml("<u>$url</u>")

        dialog.dialog_close.setOnClickListener { dialog.dismiss() }
        dialog.txt_dialog_credits_value.setOnClickListener {

            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            context.startActivity(browserIntent)

        }
        dialog.show()

    }

    //    Initialize view ID's
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rounded_image = itemView.rounded_image
        val txt_rowimageslist_id = itemView.txt_rowimageslist_id
        val txt_rowimageslist_author = itemView.txt_rowimageslist_author
        val txt_rowimageslist_credits = itemView.txt_rowimageslist_credits
    }

    public fun UpdateData(list: ArrayList<ImagesModelItem>) {
        this.imageList = list
        notifyDataSetChanged()
    }

}
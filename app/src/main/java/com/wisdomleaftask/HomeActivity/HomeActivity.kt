package com.wisdomleaftask.HomeActivity

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.applaunch.eventweather.Adapter.ImageListAdapter
import com.applaunch.stageacedemy.Retrofit.RetrofitClient
import com.wisdomleaftask.Model.ImagesModel
import com.wisdomleaftask.Model.ImagesModelItem
import com.wisdomleaftask.R
import kotlinx.android.synthetic.main.activity_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HomeActivity : AppCompatActivity() {
    private lateinit var adapter: ImageListAdapter
    private var page = 1
    var list = ArrayList<ImagesModelItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        // Verify internet connectivity

        if (verifyAvailableNetwork(this)) {
            setupSwipeRefresh()
            callImagesListAPI(1, true)
            recyclerBottom()
        } else {
            //  If there is no connection then show a popup

            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.attention))
            builder.setMessage(getString(R.string.please_check_your_connection))
            builder.setPositiveButton(getString(R.string.okay)) { dialog, which ->
                dialog.dismiss()
                finish()
            }
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

    }

//   functionality to load new data when the list reached to last index

    private fun recyclerBottom() {

        resView_homeactivty.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager =
                    LinearLayoutManager::class.java.cast(recyclerView.layoutManager)
                val totalItemCount = layoutManager!!.itemCount
                val lastVisible = layoutManager.findLastVisibleItemPosition()
                val endHasBeenReached = lastVisible + 5 >= totalItemCount
                if (totalItemCount > 0 && endHasBeenReached) {
                    callImagesListAPI(page, false)
                    page += 1
                }
            }
        })
    }

//  swipe to refresh functionality

    private fun setupSwipeRefresh() {

        swiperefresh_homeactivity.isRefreshing = true
        swiperefresh_homeactivity.setOnRefreshListener {
            callImagesListAPI(1, true)
        }
    }

//    Parse the API with the help Retrofit and image model and by passing page and the data limit.
//    Pass page count when updated data at the last index of the list.

    private fun callImagesListAPI(page: Int, isRefresh: Boolean) {

        RetrofitClient.instance.getImagesData(
            page.toString(),
            "20"
        ).enqueue(object : Callback<ImagesModel> {
            override fun onFailure(call: Call<ImagesModel>, t: Throwable) {
                Toast.makeText(this@HomeActivity, t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<ImagesModel>,
                response: Response<ImagesModel>
            ) =

                if (response.isSuccessful) {
                    val model = response.body()

                    if (isRefresh) {
                        list.clear()
                    }

                    for (i in 0 until model!!.size) {
                        val imagesModel = ImagesModelItem(
                            model[i].author,
                            model[i].download_url,
                            model[i].height,
                            model[i].id,
                            model[i].url,
                            model[i].width
                        )

                        list.add(imagesModel)

                        setImageListingRecyclerView(list, isRefresh)
                    }
                } else {
                    Toast.makeText(
                        this@HomeActivity,
                        getString(R.string.something_went_wrong),
                        Toast.LENGTH_LONG
                    ).show()
                }
        })

    }

// Show images list with the help of image list Adapter in recycler view.
// Refresh is true then whole data is updated to new data
// Refresh is false then data is added with old data

    private fun setImageListingRecyclerView(
        list: ArrayList<ImagesModelItem>,
        refresh: Boolean
    ) {
        if (refresh) {
            adapter = ImageListAdapter(this, list)
            resView_homeactivty.adapter = adapter
            swiperefresh_homeactivity.isRefreshing = false
        } else {
            adapter.UpdateData(list)
            swiperefresh_homeactivity.isRefreshing = false
        }

    }

// Check internet connectivity. Returns false if there is  not internet connection.

    fun verifyAvailableNetwork(activity: AppCompatActivity): Boolean {
        val connectivityManager =
            activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

}
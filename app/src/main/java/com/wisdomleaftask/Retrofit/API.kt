package com.applaunch.eventweather.Retrofit

import com.wisdomleaftask.Model.ImagesModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface API {

    // To call the API with its attached Model Class
    @GET("list/")
    fun getImagesData(
        @Query("page") page: String,
        @Query("limit") limit: String
    ): Call<ImagesModel>

}
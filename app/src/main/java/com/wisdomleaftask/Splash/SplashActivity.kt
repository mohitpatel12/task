package com.wisdomleaftask.Splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.wisdomleaftask.HomeActivity.HomeActivity
import com.wisdomleaftask.R
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        Full screen activity code.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_splash)

        startSplashLogoAnimation()
        startHandlerForNavigation()
    }

// Added navigation code with handler of 5 secs

    private fun startHandlerForNavigation() {
        Handler().postDelayed({
            intent = Intent(this, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()

        }, 5000)
    }

//    Added splash logo Animation for smoother transitions

    private fun startSplashLogoAnimation() {
        val animation = AnimationUtils.loadAnimation(this, R.anim.zoom_in)
        image_splash_logo.startAnimation(animation)
    }
}